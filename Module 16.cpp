﻿//Выполнение задания 16.5 о двумерном массиве

#include <iostream>
#include <time.h>
#include <locale.h>

int main()
{
    setlocale(LC_ALL, "Russian"); // Переключение на русский язык

    const int N = 5; // Размер массива определяется константой

    int array[N][N]; // Создание и заполнение двумерного массива
    for (int i = 0; i < N; ++i) // Индекс строки
    {
        for (int j = 0; j < N; ++j) // Индекс столбца
        {
            array[i][j] = i + j; // Массив, где значение = сумма индексов строки и столбца (цифра на пересечении)
        }

    }

    // Вывод двумерного массива в консоль
    std::cout << "Двумерный массив:\n";
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            std::cout << array[i][j] << " ";
        }
        std::cout << std::endl;
    }

    // Определение текущего дня месяца
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    int current_day = buf.tm_mday;

    // Вычисление индекса строки и суммы элементов в этой строке
    int row = current_day % N;
    int sum = 0;
    for (int j = 0; j < N; ++j) {
        sum += array[row][j];
    }

    // Вывод суммы элементов в строке, индекс которой равен остатку от деления текущего числа календаря на N
    std::cout << '\n' << "Сумма элементов в строке с индексом " << row << ":\n"; // Добавлен пробел после "индексом"
    for (int j = 0; j < N; ++j)
    {
        std::cout << array[row][j] << " ";
    }
    std::cout << " = " << sum << std::endl; // Добавлен пробел перед знаком равенства

    return 0;
}